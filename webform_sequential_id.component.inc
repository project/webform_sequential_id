<?php

/**
 * @file
 * Webform module sequential_id component.
 */

/**
 * Implements _webform_defaults_COMPONENT().
 */
function _webform_defaults_sequential_id() {
  return array(
    'name' => 'Sequential ID',
    'form_key' => NULL,
    'pid' => 0,
    'type' => 'sequential_id',
    'page_num' => 1,
    'weight' => 5,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'start_value' => '',
      'current_value' => '',
      'disabled' => 1,
      'unique' => 1,
      'title_display' => 0,
      'description' => '',
      'attributes' => array(),
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_theme_COMPONENT().
 */
function _webform_theme_sequential_id() {
  return array(
    'webform_display_sequential_id' => array(
      'arguments' => array('element' => NULL),
      'file' => 'webform_sequential_id.component.inc',
    ),
  );
}

/**
 * Implements _webform_edit_COMPONENT().
 *
 * @param array $component
 */
function _webform_edit_sequential_id($component) {
  $form = array();
  $form['display']['disabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disabled'),
    '#return_value' => 1,
    '#description' => t('Make this field non-editable. Useful for setting an unchangeable default value.'),
    '#weight' => 11,
    '#default_value' => $component['extra']['disabled'],
    '#parents' => array('extra', 'disabled'),
  );
  $form['validation']['start_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Start Value'),
    '#return_value' => 1,
    '#description' => t('The starting value for automatically generated sequential IDs. NOTE: Once IDs have been generated, this cannot be changed without deleting all existing submissions.'),
    '#weight' => 12,
    '#default_value' => $component['extra']['start_value'],
    '#parents' => array('extra', 'start_value'),
    '#disabled' => (webform_sequential_id_get_max_id($component) != FALSE),
  );
  $form['validation']['unique'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unique'),
    '#return_value' => 1,
    '#description' => t('Check that all entered values for this field are unique. The same value is not allowed to be used twice.'),
    '#weight' => 1,
    '#default_value' => $component['extra']['unique'],
    '#parents' => array('extra', 'unique'),
  );
  return $form;
}

/**
 * Implements _webform_render_COMPONENT().
 *
 * @param array $component
 * @param array $value
 * @param boolean $filter
 * @return array
 */
function _webform_render_sequential_id($component, $value = NULL, $filter = TRUE) {
  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#type' => 'textfield',
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before',
    '#default_value' => $filter ? _webform_filter_values($component['value'], $node, NULL, NULL, FALSE) : $component['value'],
    '#weight' => $component['weight'],
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#attributes' => $component['extra']['attributes'],
    '#theme_wrappers' => array('webform_element_wrapper'),
    '#pre_render' => array('webform_element_title_display'),
    '#post_render' => array('webform_element_wrapper'),
    '#element_validate' => array('webform_sequential_id_validate_id'),
    '#translatable' => array('title', 'description'),
  );

  if ($component['extra']['disabled']) {
    if ($filter) {
      $element['#attributes']['readonly'] = 'readonly';
    }
    else {
      $element['#disabled'] = TRUE;
    }
  }

  // Enforce uniqueness.
  if ($component['extra']['unique']) {
    $element['#element_validate'][] = 'webform_validate_unique';
  }

  // We will just show this text as the default value for the member id.
  $element['#default_value'] = (!empty($value) ? $value[0] : t('Auto Generated'));

  return $element;
}

/**
 * Implements _webform_display_COMPONENT().
 */
function _webform_display_sequential_id($component, $value, $format = 'html') {
  $component_definition = array(
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme' => 'webform_display_textfield',
    '#theme_wrappers' => $format == 'html' ? array('webform_element', 'webform_element_wrapper') : array('webform_element_text'),
    '#post_render' => array('webform_element_wrapper'),
    '#field_prefix' => (isset($component['extra']['field_prefix']) ? $component['extra']['field_prefix'] : NULL),
    '#field_suffix' => (isset($component['extra']['field_suffix']) ? $component['extra']['field_suffix'] : NULL),
    '#component' => $component,
    '#format' => $format,
    '#value' => isset($value[0]) ? $value[0] : '',
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
  return $component_definition;
}

/**
 * Format the output of data for this component.
 */
function theme_webform_display_sequential_id($element) {
  $prefix = $element['#format'] == 'html' ? filter_xss($element['#field_prefix']) : $element['#field_prefix'];
  $suffix = $element['#format'] == 'html' ? filter_xss($element['#field_suffix']) : $element['#field_suffix'];
  $value = $element['#format'] == 'html' ? check_plain($element['#value']) : $element['#value'];
  return $value !== '' ? ($prefix . $value . $suffix) : ' ';
}

/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_sequential_id($component, $sids = array(), $reset = FALSE) {
  static $rows = array();

  if (!$reset && !empty($rows)) {
    return $rows;
  }

  $submission_controller = webform_get_submission_controller();
  $result = $submission_controller->getComponentSubmissionData($component, $sids, array('data'));

  $nonblanks = 0;
  $submissions = 0;
  $minimum = NULL;
  $maximum = NULL;

  foreach ($result as $data) {
    $data = (int) trim($data['data']);
    if (drupal_strlen($data) > 0) {
      $nonblanks++;
      $minimum = ((!isset($minimum) || $data < $minimum) ? $data : $minimum);
      $maximum = ((!isset($maximum) || $data > $maximum) ? $data : $maximum);
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('Value'), $nonblanks);
  $rows[2] = array(t('Minimum'), $minimum);
  $rows[3] = array(t('Maximum'), $maximum);

  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_sequential_id($component, $value) {
  return check_plain(empty($value[0]) ? '' : $value[0]);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_sequential_id($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_sequential_id($component, $export_options, $value) {
  return !isset($value[0]) ? '' : $value[0];
}
